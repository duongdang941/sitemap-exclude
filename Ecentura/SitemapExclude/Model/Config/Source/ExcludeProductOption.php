<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ecentura\SitemapExclude\Model\Config\Source;

class ExcludeProductOption implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Get all option
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'no_index', 'label' => __('No Index')],
            ['value' => 'no_follow', 'label' => __('No Follow')],
            ['value' => 'all', 'label' => __('No Follow and No Index')],
        ];
    }
}
