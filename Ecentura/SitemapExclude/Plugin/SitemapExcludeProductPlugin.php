<?php

namespace Ecentura\SitemapExclude\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Sitemap\Model\ResourceModel\Catalog\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Psr\Log\LoggerInterface as Logger;

class SitemapExcludeProductPlugin
{
    protected const EXCLUDE_PRODUCT_OPTION_CONFIG_URL = 'sitemap/product/exclude_product_options';

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * @var ScopeConfigInterface
     */
    protected $config;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param ScopeConfigInterface $config
     * @param Logger $logger
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        ScopeConfigInterface       $config,
        Logger                     $logger
    ) {
        $this->productRepository = $productRepository;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * Exclude products have attribute no follow, no index
     *
     * @param Product $subject
     * @param array $result
     * @param int|null $storeId
     * @return array|mixed|void
     */
    public function afterGetCollection(Product $subject, $result, $storeId)
    {
        try {
            if (!empty($result)) {
                $excludedProducts = $this->getExcludedProducts($result, $storeId);
                $result = $this->filterExcludedProducts($result, $excludedProducts);
            }

            return $result;

        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return $result;
        }
    }

    /**
     * Get the list of products to be excluded based on specified criteria.
     *
     * @param array $products The original collection of products.
     * @param int $storeId The store ID.
     * @return array The list of excluded product keys.
     */
    private function getExcludedProducts($products, $storeId)
    {
        $excludedProducts = [];
        $excludedProductsOption = $this->config->getValue(self::EXCLUDE_PRODUCT_OPTION_CONFIG_URL);

        foreach ($products as $key => $productData) {
            $product = $this->productRepository->getById($productData->getData('id'), false, $storeId);

            switch ($excludedProductsOption) {
                case 'all':
                    if ($this->isProductExcludedForAll($product)) {
                        $excludedProducts[] = $key;
                    }
                    break;
                case 'no_index':
                    if ($this->isProductExcludedForNoIndex($product)) {
                        $excludedProducts[] = $key;
                    }
                    break;
                case 'no_follow':
                    if ($this->isProductExcludedForNoFollow($product)) {
                        $excludedProducts[] = $key;
                    }
                    break;

                default:
                    break;
            }
        }

        return $excludedProducts;
    }

    /**
     * Check if a product should be excluded for the 'all' option.
     *
     * @param Product $product The product to check.
     * @return bool True if the product should be excluded, false otherwise.
     */
    private function isProductExcludedForAll($product)
    {
        return $product->getData('follow_index_status')
            && !$product->getData('follow_value')
            && !$product->getData('index_value');
    }

    /**
     * Check if a product should be excluded for the 'no_index' option.
     *
     * @param Product $product The product to check.
     * @return bool True if the product should be excluded, false otherwise.
     */
    private function isProductExcludedForNoIndex($product)
    {
        return $product->getData('follow_index_status')
            && !$product->getData('index_value');
    }

    /**
     * Check if a product should be excluded for the 'no_follow' option.
     *
     * @param Product $product The product to check.
     * @return bool True if the product should be excluded, false otherwise.
     */
    private function isProductExcludedForNoFollow($product)
    {
        return $product->getData('follow_index_status')
            && !$product->getData('follow_value');
    }

    /**
     * Filter out the excluded products from the original collection.
     *
     * @param array $products The original collection of products.
     * @param array $excludedKeys The keys of products to be excluded.
     * @return array The filtered collection of products.
     */
    private function filterExcludedProducts($products, $excludedKeys)
    {
        return array_diff_key($products, array_flip($excludedKeys));
    }
}
